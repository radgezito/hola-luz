<?php

namespace Tests\Feature;

use App\Http\Controllers\ReadFileController;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GenerateCsvFileTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_read_existing_file()
    {

        var_dump('2016-readings.xml');

        $file = '2016-readings.xml';
        $readFile =  new ReadFileController();
        $content = $readFile->read($file);

        // $this->assertEquals();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_read_not_existing_file()
    {

         var_dump('xxxxxxxxx');

         $file = 'test_not_exists.csv';
         $readFile =  new ReadFileController();
         $content = $readFile->read($file);

         dd($content);

         // $this->assertEquals();
    }
}
