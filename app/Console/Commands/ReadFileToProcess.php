<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Interfaces\ReadFile;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ReadFileController;

class ReadFileToProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:readfile {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read values from files';


    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        $fileName = $this->argument('filename');

        if(!Storage::disk('public')->exists($fileName)) {
            $this->info('File doesn\'t exists');
            return false;
        }

        $extension = \File::extension($fileName);

        if (!in_array($extension,['csv','xml'])) {
            $this->info('File extension not permitted');
        }

        $fileToRead = new ReadFileController();
        $content = $fileToRead->read($fileName);

        $values = $content[0];
        $median = $content[1];

        $response = $fileToRead->outputResult($values, $median);

        foreach ($response as $item) {
            $this->info($item);
        }

    }

}
