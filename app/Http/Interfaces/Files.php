<?php

namespace App\Http\Interfaces;

interface Files
{
    public function read($filename);
}
