<?php

namespace App\Http\Classes;

use App\Http\Interfaces\Files;

class XmlFileClass implements Files
{

    /**
     * @param $filename
     * @return array
     * @throws \Exception
     */
    public function read($filename)
    {

        $linesToReturn = [];

        $filename2 = storage_path('app/public' . '/' . $filename);

        $xmlString = file_get_contents($filename2);

        $xml = new \SimpleXMLElement($xmlString);

        $count = $readingTotal = 0;

        foreach ($xml->children() as $hijo) {

            $reading = (int) $hijo->__toString();
            $readingTotal += $reading;
            $count++;

        }


        $avgReadings = number_format($readingTotal / $count, 2, '.', '');

        $minimumValue = $avgReadings - $avgReadings/2;
        $maximumValue = $avgReadings + $avgReadings/2;

        foreach ($xml->children() as $hijo) {

            $clientID = $hijo['clientID']->__toString();
            $period = $hijo['period']->__toString();
            $reading = (int) $hijo;

            if ($reading <= $minimumValue || $reading >= $maximumValue) {
                $linesToReturn[] = [
                  $clientID , $period , $reading
                ];
            }

        }

        return [$linesToReturn , $avgReadings];

    }

}
