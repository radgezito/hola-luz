<?php

namespace App\Http\Classes;

use App\Http\Interfaces\Files;

class CsvFileCLass implements Files
{
    /**
     * @param $filename
     * @return array
     */
    public function read($filename)
    {

        $file  = fopen(storage_path('app/public'.'/'.$filename), "r");
        $file2 = fopen(storage_path('app/public'.'/'.$filename), "r");

        $linesToReturn = [];

        $count = $readingTotal = 0;
        $count2 = 0;

        while ( ($data = fgetcsv($file, 200, ",")) !==FALSE ) {
            $count++;

            if ($count === 1) {
                continue;
            }

            $readingTotal += $data[2];
        }

        $avgPeriod =(int) number_format($readingTotal / $count, 2, '.', '');

        $minimumValue = $avgPeriod - ($avgPeriod/2);
        $maximumValue = $avgPeriod + ($avgPeriod/2);

        while ( ($data2 = fgetcsv($file2, 200, ",")) !==FALSE ) {

            $count2++;

            if ($count2 === 1) {
                continue;
            }

            $value = (int)$data2[2];

             if ($value <= $minimumValue || $value >= $maximumValue ) {
                 $linesToReturn[] = $data2;
             }

        }

        return [$linesToReturn, $avgPeriod];
    }

}
