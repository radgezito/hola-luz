<?php

namespace App\Http\Controllers;

use App\Http\Classes\CsvFileCLass;
use App\Http\Classes\XmlFileClass;

class ReadFileController extends Controller
{

    /**
     * @param $filename
     * @return array|null
     * @throws \Exception
     */
    public function read($filename)
    {
        $content = null;
        $extensio = \File::extension($filename);

        if ($extensio === 'csv') {
             $csvFile = new CsvFileCLass();
             $content = $csvFile->read($filename);
        }
        else if ($extensio === 'xml') {
            $xmlFile = new XmlFileClass();
            $content = $xmlFile->read($filename);
        }

        return $content;

    }


    /**
     * @param $content
     * @param $median
     * @return array
     */
    public function outputResult($content, $median)
    {

        $arrayToReturn = [];

        $arrayToReturn [] = '| Client              | Month              | Suspicious         | Median';

        foreach ($content as $cont) {
            $client = $cont[0];
            $month = substr($cont[1],5,2);
            $reading = $cont[2];
            $line =  '| '. $client . '       |   ' . $month . '               | '.$reading . '           |  '.$median;
            $arrayToReturn [] =  $line;
        }

        return $arrayToReturn;
    }

}
