
The application can be run with docker-compose or without laravel's own web server or even apache.<br>
The project is based of a command, currently prepared to receive a csv or xml file as a parameter. The command verifies that the file exists and is accessible in order to process it later.


### Setup
In order to use this laravel project needs to run this command before
<ul>
<li>composer install</li>
</ul>

### Example command
php artisan command:readfile 2016-readings.csv<br>
php artisan command:readfile 2016-readings.xml


### Laravel 9 Dockerfile
https://github.com/k90mirzaei/laravel9-docker
